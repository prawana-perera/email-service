# Email Service

## Problem Description

This is a service which would allow customers to send emails. This service makes use of various email service providers. In order to reduce inconvenience or downtime for customers in the case where a service provider is unavailable, our service makes use of several providers and gracefully switches to an available provider should one fail.

## Solution Overview

The solution involves a REST API allowing client applications to use the API to send emails. Due to time constraints, the first iteration of this API would support only the following features:

* Support a non authenticated send email API endpoint that accepts:
    * Email subject
    * Email body
    * One or more recipients
* Support validation of API inputs:
    * Valid recipient email addresses
    * Subject and body optional
* Support only plain text email body
* Support multiple backend email providers
* Support graceful failover/switching between backend providers in case one is down
* Support a static "from email address", due to some email service providers restricting the use of "unverified" sending domains. Due to time allocated, there was not enough time setup this.

### Technical Details

The API service is built using SpringBoot. A single `EmailServiceController` serves the `/api/emails` end point which support a `POST` operation to send emails. The API consumes and produces `application/json` content type.

Along with the running API service, a API documentation is also produced in Swagger 2 (OpenAPI) format. This documentation is also deployed along with the service at `/v2/api-docs`.

In the first iteration two email service providers were used (SparkPost and SendGrid). The code abstracts the implementation details of each provider behind a `EmailBackendProvider` interface, which exposes a `sendEmail` method. In addition the application loads all configured providers and provides them to the main `EmailService`, which attempts to send. The first iteration uses a simple random shuffle of the provider list so subsequent calls to send emails may yield different order of providers to try to send with. Each of the providers supplied a Java API, which was chosen as the integration mechanism.

The service was tested with both unit and integration tests. The unit tests cover majority of the functionality. There is a integration test `EmailServiceControllerIntegrationTest` which demonstrates the use of the API.

Other technical details include:

* Basic API input validation is performed on the input to the API using JSR-303 Bean validation with customised API errors
* Use of Lombok to reduce Java code boiler plates in classes
* Use of JUnit 5, Spring Mock MVC tests and end to end SpringBoot tests
* Use of `springfox-swagger2` to generate SwaggerDoc
* Test code coverage reports are available via jacoco

## Local Setup

The following is required:

* Java 8 is required, ensure the JAVA_HOME environment variable is set and the `java` executable is on the path.
* API key for both SparkPost and SendGrid email services

To get started, after cloning the repository run the following command:
```bash
# Run all tests and build to ensure everything is ok
./gradlew build
```

You can import the project into your favourite IDE. If you are using IntelliJ please install the Lombok plugin and ensure the annotation processing setting is switched on (under Preferences > Build, Execution, Deployment > Compiler > Annotation Processors)

## Running Service

There are currently two email service providers configured. They require the following environment variables to be passed in:

* sparkPostApiKey
* sendGridApiKey

You can run the following command to start the application:
```bash
java -DsparkPostApiKey=your_key -DsendGridApiKey=your_key -jar build/libs/email-service-0.0.1-SNAPSHOT.jar
```

This will start the service on default port 8080. The API can be tested using the following cURL command:
```bash
curl -X POST \
  http://localhost:8080/api/emails \
  -H 'Content-Type: application/json' \
  -d '{"recipients": ["your.recipient@domain.com"],"subject": "Hello from my email service","body": "It works!"}'
```

Also the API documentation can be viewed at http://localhost:8080/v2/api-docs.

## Improvements

### Missing Features

As far as a email services go, this service has very limited functionality. If given more time these would be candidates for features:

* View sent emails
* Add attachments
* Add CC, BCC fields
* Support HTML content
* Customer sign-up and authentication
* Customer to register their sending domain to be used in the "from" field
* A front end (customer portal) to login and use the email service

### Technical Improvements/Features

* Add static code analysis tools to improve and monitor code quality:
    * PMD
    * FindBugs
    * Checkstyle
* Setup CI
* Dockerise the service so that it is easily deployable
* Setup deployable environments (e.g. AWS) and any deployment scripts, also add to deployment steps CI/CD pipelines
* As the API evolves there maybe more and more integration tests which may increase build time and provide slower feedback
    * Separate the build steps to run unit and integration tests using Spring profiles and reflect these separate steps the CI pipeline
* To support full CRUD around sending email features, we will require a data store to keep track of sent, pending emails. This could be easily implemented using PostGreSQL db:
    * The abstraction to the data store operations would be behind `Repository` interfaces
    * Use Spring JPA repository implementation implementing our `Repository` methods
    * For a basic implementation we could store per customer the email and delivery status for each email, thereby the customer can get a view of sent and pending emails
* To support attachments:
    * Need to consider memory requirements due to size of attachments, enforcing a size limit per email will be prudent
    * We may need to virus scan the supplied attachments, the current application architecture may need to revisited support a queued approach to sending messages, see below point
* Move to a message queue architecture:
    * Current architecture is synchronous and the various parts of sending message run in a single thread
        * If we support attachments and the number of customers grows significantly the current architecture may not allow the service to scale
    * A better architecture might be a asynchronous approach with the the email service split across several modules/services:
        * At a high level, the architecture could be composed of
            * API module - responsible for accepting requests, the API module would do some basic validation, store the email request in DB, store the attachment on a shared storage and write a message to a "pre-send" queue
            * Validator module - takes the message from the pre-send queue and performs validation on the email e.g. check for viruses in attachments, it then writes a message to the "send" queue
            * Sender Module - takes the from the send queue and attempts to send using the configured backend email service providers, also updates the state in the db
            
    * This approach gives us the following:
        * Pros:
            * More modular code, easily understood
            * Ability to scale each components depending on the bottlenecks
        * Cons:
            * Deployment complexity as there are more moving parts
            




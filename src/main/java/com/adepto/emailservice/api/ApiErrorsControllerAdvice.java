package com.adepto.emailservice.api;

import com.adepto.emailservice.exceptions.BackendProvidersUnavailableException;
import com.adepto.emailservice.models.ErrorDetail;
import com.adepto.emailservice.models.ErrorResponse;
import com.adepto.emailservice.utils.SpringBindingErrorsAdapter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Arrays;

@Slf4j
@RestControllerAdvice
public class ApiErrorsControllerAdvice {

    private static final ErrorDetail SERVICE_UNAVAILABLE_ERROR = new ErrorDetail(null, "We are unable to send your email at the moment. Please try again later.");

    private final SpringBindingErrorsAdapter bindingErrorsAdapter;

    public ApiErrorsControllerAdvice(SpringBindingErrorsAdapter bindingErrorsAdapter) {
        this.bindingErrorsAdapter = bindingErrorsAdapter;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        return bindingErrorsAdapter.convertToApiErrorResponse(ex.getBindingResult());
    }

    @ExceptionHandler(BackendProvidersUnavailableException.class)
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    public ErrorResponse handleBackendProvidersUnavailable(BackendProvidersUnavailableException ex) {
        log.error("All backend email service providers were unavailable", ex);
        return new ErrorResponse(Arrays.asList(SERVICE_UNAVAILABLE_ERROR));
    }

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse handleAllOtherErrors(Throwable t) {
        log.error("All backend email service providers were unavailable", t);
        return new ErrorResponse(Arrays.asList(SERVICE_UNAVAILABLE_ERROR));
    }
}

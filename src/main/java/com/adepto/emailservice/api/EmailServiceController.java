package com.adepto.emailservice.api;

import com.adepto.emailservice.models.ErrorResponse;
import com.adepto.emailservice.models.SendEmailRequest;
import com.adepto.emailservice.models.SendEmailResponse;
import com.adepto.emailservice.service.EmailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/emails")
@Api(value = "/api/emails", description = "Emails API", produces = "application/json")
public class EmailServiceController {
    private final EmailService emailService;

    public EmailServiceController(EmailService emailService) {
        this.emailService = emailService;
    }

    @ApiOperation(value = "send email", response = SendEmailResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Email sent to recipients", response = SendEmailResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorResponse.class),
            @ApiResponse(code = 503, message = "Cannot send email", response = ErrorResponse.class)
    })
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public SendEmailResponse sendEmail(@Valid @RequestBody SendEmailRequest sendEmailRequest) {
        return emailService.sendEmail(sendEmailRequest);
    }
}

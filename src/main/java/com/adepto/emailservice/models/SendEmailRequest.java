package com.adepto.emailservice.models;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SendEmailRequest {
    @ApiModelProperty(notes = "The subject of the email")
    private String subject;

    @ApiModelProperty(notes = "The email message body, only plain text is supported")
    private String body;

    @ApiModelProperty(notes = "A list of valid email recipients")
    @NotEmpty(message = "Must have at least one to email address.")
    private List<@NotBlank(message = "Recipient email address cannot be blank.") @Email(message = "Not a valid email address.") String> recipients;
}

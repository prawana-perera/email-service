package com.adepto.emailservice.models;

import io.swagger.annotations.ApiModelProperty;
import lombok.Value;

import java.util.List;

@Value
public class ErrorResponse {
    @ApiModelProperty(notes = "A list of errors associated with the request")
    private final List<ErrorDetail> errors;
}

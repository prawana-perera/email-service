package com.adepto.emailservice.models;

import lombok.Value;

import java.util.List;

@Value
public class SendEmailResponse {
    private final List<String> recipients;
    private final String message;
}

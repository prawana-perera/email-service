package com.adepto.emailservice.models;

import io.swagger.annotations.ApiModelProperty;
import lombok.Value;

@Value
public class ErrorDetail {
    @ApiModelProperty(notes = "The model field associated with the error if any")
    private final String field;

    @ApiModelProperty(notes = "A descriptive explanation of the error")
    private final String message;
}

package com.adepto.emailservice.service;

import com.adepto.emailservice.exceptions.BackendProvidersUnavailableException;
import com.adepto.emailservice.models.SendEmailRequest;
import com.adepto.emailservice.models.SendEmailResponse;
import com.adepto.emailservice.providers.EmailBackendProvider;
import com.adepto.emailservice.providers.EmailBackendProviderFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;

@Service
@Slf4j
public class EmailService {

    private final EmailBackendProviderFactory providerFactory;

    public EmailService(EmailBackendProviderFactory providerFactory) {
        this.providerFactory = providerFactory;
    }

    public SendEmailResponse sendEmail(SendEmailRequest request) {
        List<EmailBackendProvider> allProviders = providerFactory.getAllProviders();

        boolean sent = false;
        Iterator<EmailBackendProvider> providerIt = allProviders.iterator();

        while (!sent && providerIt.hasNext()) {
            EmailBackendProvider provider = providerIt.next();
            sent = provider.sendEmail(request);
        }

        if (!sent) {
            log.error("All email backend providers were not available to send.");
            throw new BackendProvidersUnavailableException();
        }

        return new SendEmailResponse(request.getRecipients(), "Email successfully sent to recipients.");
    }
}
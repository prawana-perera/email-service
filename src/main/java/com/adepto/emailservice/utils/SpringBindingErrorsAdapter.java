package com.adepto.emailservice.utils;

import com.adepto.emailservice.models.ErrorDetail;
import com.adepto.emailservice.models.ErrorResponse;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class SpringBindingErrorsAdapter {
    public ErrorResponse convertToApiErrorResponse(BindingResult bindingResult) {
        List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        Stream<ErrorDetail> fieldErrorStream = fieldErrors
                .stream()
                .map(error -> new ErrorDetail(error.getObjectName() + "." + error.getField(), error.getDefaultMessage()));


        List<ObjectError> globalErrors = bindingResult.getGlobalErrors();
        Stream<ErrorDetail> globalErrorStream = globalErrors
                .stream()
                .map(error -> new ErrorDetail(error.getObjectName(), error.getDefaultMessage()));

        List<ErrorDetail> allApiErrors = Stream.concat(fieldErrorStream, globalErrorStream).collect(Collectors.toList());

        return new ErrorResponse(allApiErrors);
    }
}

package com.adepto.emailservice.providers;

import com.adepto.emailservice.models.SendEmailRequest;

public interface EmailBackendProvider {
    boolean sendEmail(SendEmailRequest sendEmailRequest);

    String getName();
}

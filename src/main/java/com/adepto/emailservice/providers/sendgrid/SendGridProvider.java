package com.adepto.emailservice.providers.sendgrid;

import com.adepto.emailservice.models.SendEmailRequest;
import com.adepto.emailservice.providers.EmailBackendProvider;
import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Personalization;
import com.sendgrid.Request;
import com.sendgrid.SendGrid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Profile("!test")
@Slf4j
public class SendGridProvider implements EmailBackendProvider {

    public static final String PROVIDER_NAME = "SendGrid";
    private static final Email FROM = new Email("test@example.com");
    private final SendGrid client;

    public SendGridProvider(SendGrid client) {
        this.client = client;
    }

    @Override
    public boolean sendEmail(SendEmailRequest sendEmailRequest) {
        Mail mail = new Mail();
        mail.setFrom(FROM);
        mail.setSubject(sendEmailRequest.getSubject());
        mail.addContent(new Content("text/plain", sendEmailRequest.getBody()));

        sendEmailRequest.getRecipients()
                .stream()
                .map(to -> {
                    Personalization personalization = new Personalization();
                    personalization.addTo(new Email(to));
                    return personalization;
                })
                .forEach(mail::addPersonalization);


        Request sendGridRequest = new Request();

        try {
            sendGridRequest.setMethod(Method.POST);
            sendGridRequest.setEndpoint("mail/send");
            sendGridRequest.setBody(mail.build());
            client.api(sendGridRequest);

            return true;
        } catch (IOException e) {
            log.error("An error occurred sending via SendGrid", e);
            return false;
        }
    }

    @Override
    public String getName() {
        return PROVIDER_NAME;
    }
}

package com.adepto.emailservice.providers.sendgrid;

import com.sendgrid.SendGrid;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotBlank;

@Configuration
public class SendGridConfiguration {

    @NotBlank
    @Value("${sendGridApiKey}")
    private String apiKey;

    @Bean
    public SendGrid sendGridClient() {
        return new SendGrid(apiKey);
    }
}

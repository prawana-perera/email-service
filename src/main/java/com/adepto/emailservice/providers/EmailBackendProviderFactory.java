package com.adepto.emailservice.providers;

import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A factory to return create/get email backend service providers used to sending emails.
 */
@Component
public class EmailBackendProviderFactory {
    private final List<EmailBackendProvider> allProviders;

    public EmailBackendProviderFactory(List<EmailBackendProvider> allProviders) {
        this.allProviders = allProviders;
    }

    /**
     * Returns all the providers, each time this is called the providers are shuffled so that no provider is preferred over the other in
     * subsequent calls to send emails.
     *
     * @return
     */
    public List<EmailBackendProvider> getAllProviders() {
        List<EmailBackendProvider> allProviderCopy = allProviders.stream().collect(Collectors.toList());
        Collections.shuffle(allProviderCopy);

        return Collections.unmodifiableList(allProviders);
    }
}

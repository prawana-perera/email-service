package com.adepto.emailservice.providers.sparkpost;

import com.adepto.emailservice.models.SendEmailRequest;
import com.adepto.emailservice.providers.EmailBackendProvider;
import com.sparkpost.Client;
import com.sparkpost.exception.SparkPostException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("!test")
@Slf4j
public class SparkPostProvider implements EmailBackendProvider {
    public static final String FROM = "testing@sparkpostbox.com";
    public static final String PROVIDER_NAME = "SparkPost";

    private final Client client;

    public SparkPostProvider(Client client) {
        this.client = client;
    }

    @Override
    public boolean sendEmail(SendEmailRequest sendEmailRequest) {
        try {
            client.sendMessage(
                    FROM,
                    sendEmailRequest.getRecipients(),
                    sendEmailRequest.getSubject(),
                    sendEmailRequest.getBody(),
                    null
            );
            return true;
        } catch (SparkPostException e) {
            log.error("An error occurred sending via SendGrid", e);
            return false;
        }
    }

    @Override
    public String getName() {
        return PROVIDER_NAME;
    }
}

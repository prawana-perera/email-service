package com.adepto.emailservice.providers.sparkpost;

import com.sparkpost.Client;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotBlank;

@Configuration
public class SparkPostConfiguration {

    @NotBlank
    @Value("${sparkPostApiKey}")
    private String apiKey;

    @Bean
    public Client sparkPostClient() {
        return new Client(apiKey);
    }
}

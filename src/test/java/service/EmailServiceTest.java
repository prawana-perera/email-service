package service;

import com.adepto.emailservice.exceptions.BackendProvidersUnavailableException;
import com.adepto.emailservice.models.SendEmailRequest;
import com.adepto.emailservice.models.SendEmailResponse;
import com.adepto.emailservice.providers.EmailBackendProvider;
import com.adepto.emailservice.providers.EmailBackendProviderFactory;
import com.adepto.emailservice.service.EmailService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class EmailServiceTest {

    private EmailService service;

    @Mock
    private EmailBackendProviderFactory factory;

    @Mock
    private EmailBackendProvider providerOne;

    @Mock
    private EmailBackendProvider providerTwo;

    private List<EmailBackendProvider> providers;
    private SendEmailRequest sendEmailRequest;

    @BeforeEach
    public void setUp() {
        sendEmailRequest = SendEmailRequest.builder()
                .recipients(Arrays.asList("test@test.com"))
                .build();

        providers = Arrays.asList(providerOne, providerTwo);
        when(factory.getAllProviders()).thenReturn(providers);

        service = new EmailService(factory);
    }

    @Test
    @DisplayName("should send email using backend provider")
    public void sendEmail() {
        when(providerOne.sendEmail(sendEmailRequest)).thenReturn(true);

        SendEmailResponse response = service.sendEmail(sendEmailRequest);
        assertThat(response, is(notNullValue()));
        assertThat(response.getMessage(), is("Email successfully sent to recipients."));
        assertThat(response.getRecipients(), is(sendEmailRequest.getRecipients()));
    }

    @Test
    @DisplayName("should automatically use next available provider when one fails to send")
    public void sendEmailProviderFailure() {
        when(providerOne.sendEmail(sendEmailRequest)).thenReturn(false);
        when(providerTwo.sendEmail(sendEmailRequest)).thenReturn(true);

        SendEmailResponse response = service.sendEmail(sendEmailRequest);
        assertThat(response, is(notNullValue()));
        assertThat(response.getMessage(), is("Email successfully sent to recipients."));
        assertThat(response.getRecipients(), is(sendEmailRequest.getRecipients()));
    }

    @Test
    @DisplayName("should throw an exception when no providers are able to send")
    public void sendEmailAllProviderFailure() {
        when(providerOne.sendEmail(sendEmailRequest)).thenReturn(false);
        when(providerTwo.sendEmail(sendEmailRequest)).thenReturn(false);
        assertThrows(BackendProvidersUnavailableException.class, () -> {
            service.sendEmail(sendEmailRequest);
        });
    }
}
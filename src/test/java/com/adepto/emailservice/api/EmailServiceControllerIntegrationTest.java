package com.adepto.emailservice.api;

import com.adepto.emailservice.EmailServiceApplication;
import com.adepto.emailservice.models.SendEmailRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = EmailServiceApplication.class)
@TestPropertySource("classpath:application-test.yml")
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class EmailServiceControllerIntegrationTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mvc;

    @Test
    @DisplayName("should send an email")
    public void shouldReturnResponse() throws Exception {
        SendEmailRequest sendEmailRequest = SendEmailRequest.builder()
                .subject("testing 123")
                .body("Please ignore this.")
                .recipients(Arrays.asList("test@test2.com"))
                .build();

        RequestBuilder apiRequest = post("/api/emails")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(sendEmailRequest));
        mvc
                .perform(apiRequest)
                .andExpect(status().isOk())
                .andExpect(jsonPath("message", Matchers.is("Email successfully sent to recipients.")))
                .andExpect(jsonPath("recipients", hasSize(1)))
                .andExpect(jsonPath("recipients[0]", Matchers.is(sendEmailRequest.getRecipients().get(0))));
    }

    @Test
    @DisplayName("should return errors when no recipients")
    public void shouldReturnErrorResponseWhenNoRecipients() throws Exception {
        SendEmailRequest sendEmailRequest = SendEmailRequest.builder()
                .subject("testing 123")
                .body("Please ignore this.")
                .build();

        RequestBuilder apiRequest = post("/api/emails")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(sendEmailRequest));
        mvc
                .perform(apiRequest)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors", hasSize(1)))
                .andExpect(jsonPath("errors[0].field", is("sendEmailRequest.recipients")))
                .andExpect(jsonPath("errors[0].message", is("Must have at least one to email address.")));
    }

    @Test
    @DisplayName("should return errors when invalid recipient email address")
    public void shouldReturnErrorResponseWhenInvalidRecipientEmailAddress() throws Exception {
        SendEmailRequest sendEmailRequest = SendEmailRequest.builder()
                .subject("testing 123")
                .recipients(Arrays.asList("test_test2.com"))
                .body("Please ignore this.")
                .build();

        RequestBuilder apiRequest = post("/api/emails")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(sendEmailRequest));
        mvc
                .perform(apiRequest)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors", hasSize(1)))
                .andExpect(jsonPath("errors[0].field", is("sendEmailRequest.recipients[0]")))
                .andExpect(jsonPath("errors[0].message", is("Not a valid email address.")));
    }

    @Test
    @DisplayName("should return errors when email service cannot send")
    public void shouldReturnErrorResponseWhenEmailServiceCannotSend() throws Exception {
        SendEmailRequest sendEmailRequest = SendEmailRequest.builder()
                .subject("FAIL")
                .body("Please ignore this.")
                .recipients(Arrays.asList("test@test2.com"))
                .build();

        RequestBuilder apiRequest = post("/api/emails")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(sendEmailRequest));
        mvc
                .perform(apiRequest)
                .andExpect(status().isServiceUnavailable())
                .andExpect(jsonPath("errors", hasSize(1)))
                .andExpect(jsonPath("errors[0].field", isEmptyOrNullString()))
                .andExpect(jsonPath("errors[0].message", is("We are unable to send your email at the moment. Please try again later.")));
    }
}

package com.adepto.emailservice.api;

import com.adepto.emailservice.exceptions.BackendProvidersUnavailableException;
import com.adepto.emailservice.models.ErrorDetail;
import com.adepto.emailservice.models.ErrorResponse;
import com.adepto.emailservice.utils.SpringBindingErrorsAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ApiErrorsControllerAdviceTest {

    private ApiErrorsControllerAdvice controllerAdvice;

    @Mock
    private SpringBindingErrorsAdapter errorsAdapter;

    @Mock
    private BindingResult bindingResult;

    @BeforeEach
    public void setUp() {
        controllerAdvice = new ApiErrorsControllerAdvice(errorsAdapter);
    }

    @Test
    @DisplayName("should handle spring binding errors and return an error response")
    public void handleMethodArgumentNotValid() {
        ErrorResponse errorResponse = new ErrorResponse(Arrays.asList(
                new ErrorDetail("test", "test2")
        ));
        when(errorsAdapter.convertToApiErrorResponse(ArgumentMatchers.any(BindingResult.class))).thenReturn(errorResponse);

        ErrorResponse result = controllerAdvice.handleMethodArgumentNotValid(new MethodArgumentNotValidException(null, bindingResult));
        assertThat(result, is(errorResponse));
    }

    @Test
    @DisplayName("should handle back end provider unavailable exception and return an error response")
    public void handleBackendProvidersUnavailable() {
        ErrorResponse result = controllerAdvice.handleBackendProvidersUnavailable(new BackendProvidersUnavailableException());
        assertThat(result, is(new ErrorResponse(Arrays.asList(new ErrorDetail(null, "We are unable to send your email at the moment. Please try again later.")))));
    }

    @Test
    @DisplayName("should handle other exceptions and return an error response")
    public void handleAllOtherErrors() {
        ErrorResponse result = controllerAdvice.handleAllOtherErrors(new RuntimeException());
        assertThat(result, is(new ErrorResponse(Arrays.asList(new ErrorDetail(null, "We are unable to send your email at the moment. Please try again later.")))));
    }
}
package com.adepto.emailservice.api;

import com.adepto.emailservice.models.SendEmailRequest;
import com.adepto.emailservice.models.SendEmailResponse;
import com.adepto.emailservice.service.EmailService;
import com.adepto.emailservice.utils.SpringBindingErrorsAdapter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;

import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(EmailServiceController.class)
public class EmailServiceControllerTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private EmailService emailService;

    @MockBean
    private SpringBindingErrorsAdapter controllerAdvice;

    @Test
    @DisplayName("should send an email when valid email request")
    public void shouldReturnResponse() throws Exception {
        SendEmailRequest sendEmailRequest = SendEmailRequest.builder()
                .subject("testing 123")
                .body("Please ignore this.")
                .recipients(Arrays.asList("test@gmail.com"))
                .build();
        SendEmailResponse sendEmailResponse = new SendEmailResponse(sendEmailRequest.getRecipients(), "OK");
        when(emailService.sendEmail(sendEmailRequest)).thenReturn(sendEmailResponse);

        RequestBuilder apiRequest = post("/api/emails")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(sendEmailRequest));

        mvc
                .perform(apiRequest)
                .andExpect(status().isOk())
                .andExpect(jsonPath("message", is("OK")))
                .andExpect(jsonPath("recipients", hasSize(1)))
                .andExpect(jsonPath("recipients[0]", is(sendEmailRequest.getRecipients().get(0))));

    }
}
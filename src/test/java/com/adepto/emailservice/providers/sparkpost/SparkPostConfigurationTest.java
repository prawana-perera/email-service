package com.adepto.emailservice.providers.sparkpost;

import com.sparkpost.Client;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SparkPostConfiguration.class})
@TestPropertySource("classpath:application-test.yml")
public class SparkPostConfigurationTest {

    @Autowired
    private Client sparkPostClient;

    @Value("${sparkPostApiKey}")
    private String apiKey;

    @Test
    @DisplayName("create a configured spark post client bean")
    void sparkPostClient() {
        assertThat(sparkPostClient, is(notNullValue()));
        assertThat(sparkPostClient.getAuthKey(), is(apiKey));
    }
}
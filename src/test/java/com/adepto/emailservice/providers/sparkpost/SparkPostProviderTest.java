package com.adepto.emailservice.providers.sparkpost;

import com.adepto.emailservice.models.SendEmailRequest;
import com.sparkpost.Client;
import com.sparkpost.exception.SparkPostException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.anyList;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SparkPostProviderTest {

    private SparkPostProvider provider;

    @Mock
    private Client client;

    private SendEmailRequest sendEmailRequest;

    @BeforeEach
    public void setUp() {
        provider = new SparkPostProvider(client);

        sendEmailRequest = SendEmailRequest.builder()
                .subject("Test")
                .body("Hello")
                .recipients(Arrays.asList("test@test.com"))
                .build();
    }

    @Test
    @DisplayName("should send email using spark post")
    public void sendEmail() throws Exception {
        boolean result = provider.sendEmail(sendEmailRequest);
        assertThat(result, is(true));
        verify(client, times(1)).sendMessage(
                SparkPostProvider.FROM,
                sendEmailRequest.getRecipients(),
                sendEmailRequest.getSubject(),
                sendEmailRequest.getBody(),
                null);
    }

    @Test
    @DisplayName("should handle and return when spark post failure")
    public void sendEmailFailure() throws Exception {
        when(client.sendMessage(anyString(), anyList(), anyString(), anyString(), eq(null)))
                .thenThrow(new SparkPostException("error"));

        boolean result = provider.sendEmail(sendEmailRequest);
        assertThat(result, is(false));
    }

    @Test
    @DisplayName("should return name of provider")
    public void getName() throws Exception {
        assertThat(provider.getName(), is("SparkPost"));
    }
}
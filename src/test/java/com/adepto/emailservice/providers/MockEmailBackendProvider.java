package com.adepto.emailservice.providers;

import com.adepto.emailservice.models.SendEmailRequest;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("test")
public class MockEmailBackendProvider implements EmailBackendProvider {
    @Override
    public boolean sendEmail(SendEmailRequest sendEmailRequest) {
        String subject = sendEmailRequest.getSubject();

        if ("FAIL".equals(subject)) {
            return false;
        }

        return true;
    }

    @Override
    public String getName() {
        return "TestSparkPost";
    }
}

package com.adepto.emailservice.providers;

import com.adepto.emailservice.models.SendEmailRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;


public class EmailBackendProviderFactoryTest {

    private static final List<EmailBackendProvider> PROVIDERS = Arrays.asList(
            newProvider("james"),
            newProvider("john"),
            newProvider("sarah"),
            newProvider("lucy"),
            newProvider("ann")
    );

    private EmailBackendProviderFactory factory;

    public static EmailBackendProvider newProvider(String name) {
        return new EmailBackendProvider() {
            @Override
            public boolean sendEmail(SendEmailRequest sendEmailRequest) {
                return false;
            }

            @Override
            public String getName() {
                return name;
            }
        };
    }

    @BeforeEach
    public void setUp() {
        factory = new EmailBackendProviderFactory(PROVIDERS);
    }

    @Test
    @DisplayName("should return all available providers")
    public void getAllProviders() {
        List<EmailBackendProvider> allProviders = factory.getAllProviders();
        assertThat(allProviders, is(notNullValue()));
        assertThat(allProviders, hasSize(PROVIDERS.size()));
        assertThat(allProviders, containsInAnyOrder(PROVIDERS.toArray()));
    }
}
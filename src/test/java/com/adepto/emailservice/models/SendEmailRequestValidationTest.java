package com.adepto.emailservice.models;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Arrays;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

class SendEmailRequestValidationTest {

    private static ValidatorFactory validatorFactory;
    private static Validator validator;

    @BeforeAll
    public static void beforeAll() {
        validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @AfterAll
    public static void afterAll() {
        validatorFactory.close();
    }

    @Test
    @DisplayName("should pass validation when all properties are present")
    public void validFullRequest() {
        SendEmailRequest sendEmailRequest = SendEmailRequest.builder()
                .recipients(Arrays.asList("test@test.com"))
                .body("test1")
                .subject("more test")
                .build();

        Set<ConstraintViolation<SendEmailRequest>> results = validator.validate(sendEmailRequest);
        assertThat(results, is(empty()));
    }

    @Test
    @DisplayName("should pass validation when minimum required properties are present")
    public void validMinimalRequest() {
        SendEmailRequest sendEmailRequest = SendEmailRequest.builder()
                .recipients(Arrays.asList("test@test.com"))
                .build();

        Set<ConstraintViolation<SendEmailRequest>> results = validator.validate(sendEmailRequest);
        assertThat(results, is(empty()));
    }

    @Test
    @DisplayName("should fail validation when no recipients")
    public void inValidNoRecipients() {
        SendEmailRequest sendEmailRequest = SendEmailRequest.builder()
                .recipients(null)
                .build();

        Set<ConstraintViolation<SendEmailRequest>> results = validator.validate(sendEmailRequest);
        assertThat(results, is(not(empty())));
        assertThat(results, hasSize(1));

        ConstraintViolation<SendEmailRequest> violation = results.iterator().next();
        assertThat(violation.getMessage(), is("Must have at least one to email address."));
    }

    @Test
    @DisplayName("should fail validation when invalid recipient email address")
    public void inValidEmailAddress() {
        SendEmailRequest sendEmailRequest = SendEmailRequest.builder()
                .recipients(Arrays.asList("test-test.com"))
                .build();

        Set<ConstraintViolation<SendEmailRequest>> results = validator.validate(sendEmailRequest);
        assertThat(results, is(not(empty())));
        assertThat(results, hasSize(1));

        ConstraintViolation<SendEmailRequest> violation = results.iterator().next();
        assertThat(violation.getMessage(), is("Not a valid email address."));

    }
}
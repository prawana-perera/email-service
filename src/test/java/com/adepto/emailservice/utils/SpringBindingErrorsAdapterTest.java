package com.adepto.emailservice.utils;


import com.adepto.emailservice.models.ErrorDetail;
import com.adepto.emailservice.models.ErrorResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class SpringBindingErrorsAdapterTest {

    private SpringBindingErrorsAdapter adapter;

    @Mock
    private BindingResult bindingResult;

    @BeforeEach
    public void setUp() {
        adapter = new SpringBindingErrorsAdapter();
    }

    @Test
    @DisplayName("should convert spring binding error to error response")
    public void convertToApiErrorResponse() {
        when(bindingResult.getFieldErrors()).thenReturn(Arrays.asList(
                new FieldError("user", "email", "InvalidEmail")
        ));
        when(bindingResult.getGlobalErrors()).thenReturn(Arrays.asList(
                new ObjectError("user", "DuplicateUsername")
        ));

        ErrorResponse errorResponse = adapter.convertToApiErrorResponse(bindingResult);
        assertThat(errorResponse, is(notNullValue()));
        assertThat(errorResponse.getErrors(), hasSize(2));
        assertThat(errorResponse.getErrors(), containsInAnyOrder(
                new ErrorDetail("user.email", "InvalidEmail"),
                new ErrorDetail("user", "DuplicateUsername")
        ));
    }
}